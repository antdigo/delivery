FROM --platform=${TARGETPLATFORM:-linux/amd64} alpine:3.20 as php-base

ARG APP_ENV=prod
ENV APP_ENV=${APP_ENV}

ARG COMPOSER_AUTH
ENV COMPOSER_AUTH=${COMPOSER_AUTH:-{}}

RUN set -x \
    && apk add --no-cache \
        ca-certificates \
        composer \
        curl \
        php83 \
        php83-ctype \
        php83-curl \
        php83-embed \
        php83-fileinfo \
        php83-iconv \
        php83-intl \
        php83-mbstring \
        php83-opcache \
        php83-openssl \
        php83-pecl-xdebug \
    	php83-pdo_pgsql \
        php83-posix \
        php83-session \
        php83-simplexml \
        php83-soap \
        php83-sockets \
        php83-sodium \
        php83-tokenizer \
        php83-xml \
        php83-xmlreader \
        php83-xmlwriter \
        tzdata \
        unit \
        unit-php83 \
    && composer --version \
;

COPY composer.json composer.lock symfony.lock /app/
RUN set -x \
    && cd /app/ \
    && composer install --no-scripts --prefer-dist --no-progress --no-interaction \
;

COPY . /app/
RUN set -x \
    && cd /app/ \
    && composer dump-autoload --classmap-authoritative \
    && composer check-platform-reqs \
;

RUN set -x \
    && mkdir -p /var/lib/unit/ \
    && ln -sf /dev/stdout /var/log/unit.log \
;

COPY ./.scratch/common /
COPY ./.scratch/${APP_ENV}/ /

WORKDIR /app

STOPSIGNAL SIGTERM

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["unitd", "--no-daemon", "--control", "unix:/var/run/control.unit.sock"]
